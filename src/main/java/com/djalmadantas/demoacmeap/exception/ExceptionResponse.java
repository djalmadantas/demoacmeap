package com.djalmadantas.demoacmeap.exception;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ExceptionResponse 
{
	private Date timestamp;
	private String mensagem;
	private String detalhes;
}
