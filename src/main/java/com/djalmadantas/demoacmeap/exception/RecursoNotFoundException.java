package com.djalmadantas.demoacmeap.exception;

public class RecursoNotFoundException extends RuntimeException
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2261764135803103712L;

	public RecursoNotFoundException(String message)
	{
		super(message);
		// TODO Auto-generated constructor stub
	}

}
