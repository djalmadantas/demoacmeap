package com.djalmadantas.demoacmeap.domain;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import lombok.Data;

@Entity
@Data
public class Fatura
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_instalacao")
	private Instalacao instalacao;

	private String codigo;
	private Date dataLeitura;
	private Date dataVencimento;
	private int numeroLeitura;
	private double valorConta;
}
