package com.djalmadantas.demoacmeap.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.djalmadantas.demoacmeap.domain.Fatura;
import com.djalmadantas.demoacmeap.domain.Instalacao;

public interface FaturaRepository extends JpaRepository<Fatura, Long>
{
	public Optional<Fatura> findByCodigo(String codigo);
	public List<Fatura> findByInstalacao(Instalacao instalacao);
}
